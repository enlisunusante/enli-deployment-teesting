Encounters = new Meteor.Collection("encounters");
Members = new Meteor.Collection("members");
Organization = new Meteor.Collection("organization");
Claims = new Meteor.Collection("claims");
CareSheet = new Meteor.Collection("careSheets");

CareSheet.schema = new SimpleSchema({
  memberId:{
    type: String,
    label: "Member Id",
    max:100
  },
  dateOfEncounter:{
    type: String,
    label:"Date of Encounter",
  },
  firstName:{
    type: String,
    label:"First Name",
    max:100
  },
  lastName:{
    type: String,
    label:"First Name",
    max:100
  },
  address1:{
    type: String,
    label:"Address 1",
    max:100
  },
  address2:{
    type: String,
    label:"Address 2",
    max:100
  },
  address3:{
    type:String,
    label:"Address 2",
    max:100
  },
  city:{
    type:String,
    label:"City",
    max:100
  },
  zipCode:{
    type:String,
    label:"Zip Code",
    max:50
  },
  dob:{
    type:Date,
    label:"Date of Birth",
  },
  gender:{
    type:String,
    label:"Gender",
    allowedValues:["male","female"]
  },
  physicianName:{
    type:String,
    label:"Physician Name",
    max:100
  },
  illness:{
    type:String,
    label:"Injury/Illness",
    max:500
  },
  allergies:{
    type:String,
    label:"Allergies",
    max:100
  },
  clinicalInfo:{
    type:String,
    label:"Clinical Information",
    max:300
  },
  thirdPartyBilling:{
    type:Boolean,
    label:"Third Party Billing"
  },
  careSheetCertified:{
    type:Boolean,
    label:"Care Sheet certified"
  },
  encounterId:{
    type:Object
  },
  createdDate:{
    type:Date,
    label:"Create Date",
    autoValue:function(){
      if(this.isInsert){
        return new Date();
      }
    }
  },
  updatedDate:{
    type:Date,
    label:"Updated Date",
    autoValue: function(){
      if(this.isUpdate){
        return new Date();
      }
    }
  }
})
