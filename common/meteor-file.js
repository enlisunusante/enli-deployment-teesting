function defaultZero(value){
  return _.isUndefined(value) ? 0:value;
}

MeteorFile = function(options){
  options = options || {};
  this.name = options.name;
  this.type = options.type;
  this.size = options.size;
  this.data = options.data;
  this.start = defaultZero(options.start);
  this.end = defaultZero(options.end);
  this.bytesRead = this.start = defaultZero(options.bytesUploaded);
  this._id = options._id || Meteor.uuid();
};

MeteorFile.fromJSONValue = function(value){
  return new MeteorFile({
    name: value.name,
    type:value.type,
    size: value.size,
    data: EJSON.fromJSONValue(value.data),
    start: value.start,
    end: value.end,
    bytesRead: value.bytesRead,
    bytesUploaded: value.bytesUploaded,
    _id: value._id
  });
};

MeteorFile.prototype={
  constructor: MeteorFile,
  typeName: function(){
    return "MeteorFile";
  },
equals: function(other){
    return
      this._id == other._id;

  },

clone: function(){
    return new MeteorFile({
      name: this.name,
      type:this.type,
      size: this.size,
      data: this.data,
      start: this.start,
      end: this.end,
      bytesRead: this.bytesRead,
      bytesUploaded: this.bytesUploaded,
      _id: this._id
    });
  },
toJSONValue: function(){
  return {
    name: this.name,
    type:this.type,
    size : this.size,
    data:EJSON.toJSONValue(this.data),
    start: this.start,
    end: this.end,
    bytesRead: this.bytesRead,
    bytesUploaded: this.bytesUploaded,
    _id: this._id
  };
}

};

EJSON.addType("MeteorFile", MeteorFile.fromJSONValue);

if (Meteor.isClient){
  _.extend(MeteorFile.prototype,{
    read: function (file,options, callback){

      if (arguments.length == 2)
        callback = options;

        options = options ||{};
      var reader = new FileReader;
      var self = this;
      var chunkSize = options.size || 1024;
      callback = callback || function () {};
      self.size = file.size;
      self.start = self.end;
      self.end += chunkSize;

      if (self.end > self.size)
       self.end = self.size;
      reader.onload = function(){
        self.bytesRead += self.end - self.start;
        self.data = new Uint8Array(reader.result);
        callback(null, self);
      };
      reader.onerror = function(){
        callback && callback(reader.error);
      };
      if ((this.end - this.start) > 0){
        var blob = file.slice(self.start, self.end);
        reader.readAsArrayBuffer(blob);
      }
      return this;
    },
    rewind: function(){
      this.data = null;
      this.start =  0;
      this.end = 0;
      this.bytesRead = 0;
      this.bytesUploaded = 0;
    },
    upload: function (file,method,options,callback){
      console.log("insided the upload method");
        console.log("insided the upload method");
      var self = this;
      if (!Blob.prototype.isPrototypeOf(file))
        throw new Meteor.Error("First Parameter must inherit from Blob");

      if (!_.isString(method))
        throw new Meteor.Error("Second Parameter must be a Meteor.Method name");

      if(arguments.length < 4 && _.isFunction(options)){
        callback = options;
        options = {};
      }
      options = options || {};
      self.rewind();
      self.size = file.size;

      var readNext= function (){
        if (self.bytesUploaded < self.size){
          self.read(file, options, function(err,res){
            if (err && callback)
              callback(err);
            else if (err)
              throw err;
            else{
              Meteor.apply(
                method,
                [self].concat(options.param||[]),
                {
                  wait: true
                },
                function(err){
                  if(err && callback)
                  callback(err);
                  else if(err)
                  throw err;
                  else {
                      self.bytesUploaded += self.data.length;
                    readNext();
                  }
                }
              );
            }

          });
        }else{
          callback && callback(null,self);
        }
      }
      readNext();
      return this;
    }
  });

  _.extend(MeteorFile,{
    read: function(file, callback){
      console.log("insided the read");
      return new MeteorFile(file).read(file,options,callback);
    },
    upload: function(file,method,options,callback){
      console.log("insided the upload");
      return new MeteorFile(file).upload(file,method,options,callback);
    }
  })
}

if (Meteor.isServer){
  var fs = Npm.require('fs');
  var path = Npm.require('path');

  function sanitize (fileName){
    return fileName .replace(/\//g,  '')
    .replace(/\.\.+/g, '.')
  }
  _.extend(MeteorFile.prototype,{
    save: function(dirPath,options){
      var filePath = path.join(dirPath, sanitize(this.name));
      var buffer = new Buffer(this.data);
      fs.writeFileSync(filePath,buffer,options);
      var mode = this.start == 0 ? 'w' : 'a';
      var fd = fs.openSync(filePath,mode);
      fs.writeSync(fd, buffer, 0, buffer.length,this.start);
      fs.closeSync(fd);
    }
  })
}
