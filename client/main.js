
Meteor.subscribe("EncounterList");
var encounterList = new ReactiveVar([]);

$('.top.menu .item').tab();
Template.claim.events({
   'click #tabularmenu': function(event,template) {
     event.preventDefault();
      $('.item').removeClass('active');
      $(event.target).addClass('active');
    }
});

Template.claimList.events({
  'click #addClaimButton': function(event,template){
    event.preventDefault();
    Session.set("activeModal",'selectencounter' );
    $('.ui.modal').modal({closable:false,detachable:false, observeChanges:true} ).modal('show').modal('refresh');
    },
});

Template.selectencounter.helpers({
  list: function(){
    return encounterList.get();
  },
  hasItem: function(){
    var hasItem =  encounterList.get().count();
    return hasItem;
  },
  humanReadableDate: function(date){
    var m = moment(date);
    return m.format("MMM,DD YYYY HH:mm")
  }
});

Template.claimList.onRendered(function(){
  // No check - user is present
  Tracker.autorun(function () {
  $('.menu .item').tab({});
});

})


Template.selectencounter.onRendered(function(){
  search = {};
  $('body').delegate('.ui.dropdown', 'click', function(event) {
    $('.ui.dropdown').dropdown({
        transition: 'drop'
      });
    });

  search.status = "CareSheet prepared";
  encounterList.set(Encounters.find(search));
});

Template.selectencounter.events({
  'click #cancel': function(event,template){
    event.preventDefault();
    $('.ui.modal')
      .modal('hide');
   },

  'click #next': function(event,template){
    event.preventDefault();
    var selected;
    selected = template.find('input:radio[name=selectencounterfield]:checked');
    if (selected)
    {
      Session.set("encounterId",selected.id);
      Session.set("activeModal",'caresheetpopup');

    }
    else{
      alert("Please select a patient");
    }

  }
});

Template.modal.helpers({
  activeModal: function() {
    return Session.get('activeModal');
  }
});

Template.modal.created = function functionName() {
  Session.set("activeModal",'selectencounter' );

}

Template.claimprocess.events({
   'click #claimMenu': function(event,template) {
     event.preventDefault();
      $('.item').removeClass('active');
      $(event.target).addClass('active');
    }
});

Template.caresheetpopup.events({
"click .next-btn": function(event, template){
  event.preventDefault();

  var fileInput = template.find('input[type=file]');
  var form = event.currentTarget;
  var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
  var file = fileInput.files[0];
  var fileType = file["type"];
  if ($.inArray(fileType, ValidImageTypes) < 0)
  {
    alert("Invalid File uploaded. Should be an image");
  }else{
  MeteorFile.upload(file,"uploadFile",{size:1024*1024},function(err){
      if (err)
        throw err;
      else
        console.log("Upload Successful");
        form.reset();
    });
    }
  },


  "change #file-upload-input": function(event, template){
    var file = event.currentTarget.files[0];
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    var form = event.currentTarget;
    if ($.inArray(fileType, ValidImageTypes) < 0)
    {
      alert("Invalid File uploaded. Should be an image");
      form.reset();

    }else{
      $('#fileInputText').val(file["name"]);
      var image = document.getElementById('careSheetImg');
      image.src = URL.createObjectURL(file);
    }
  },

  "click .cancel-btn": function(event,template){
    event.preventDefault();
    $('.ui.modal')
      .modal('hide');

  },

  "click .edit-caresheet": function(event,template){
    event.preventDefault();
    $('.ui.modal').modal({closable:false,detachable:false, observeChanges:true} ).modal('show');
  //  $('#caresheetedit').modal({closable:false,detachable:true, observeChanges:true} ).modal('show');
  }

});


Template.caresheetedit.events({
"click #cancel": function(event, template){
  event.preventDefault();
  $('#caresheetdetails')
    .modal('hide');
  },

  "submit form": function(event, template){
    event.preventDefault();
    var careSheet = {
      memberId : event.target.memberId.value,
      firstName : event.target.firstName.value,
      lastName : event.target.lastName.value,
      address1 : event.target.address1.value,
      address2 : event.target.address2.value,
      address3 : event.target.address3.value,
      city : event.target.city.value,
      zipcode : event.target.zipcode.value,
      dateofbirth : event.target.dateofbirth.value,
      illness : event.target.compliants.value
    } ;
    Caresheet.insert(caresheet);
  }
});

Template.caresheetedit.helpers({
  memberInfo: function(){
    var encounterId = Session.get("encounterId");
    var memberdata = Encounters.findOne({'_id.str': encounterId});
    if (memberdata == null){
      alert("Encounter data not found");
    }else{
      return memberdata;
    }
  }
});

Template.caresheetedit.onRendered(function(){
  // No check - user is present
    var encounterId = Session.get("encounterId");
    var memberdata = Encounters.findOne({'_id.str': encounterId});
    if (memberdata == null){
      alert("Encounter data not found");
    }else{
      return memberdata;
    }
});
