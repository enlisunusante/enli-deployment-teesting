CareSheet = function(options){
  options = options || {};
  this.memberId = options.memberId;
  this.firstName = options.firstName;
  this.lastName = options.lastName;
  this.Address1 = options.Address1;
  this.Address2 = options.Address2;
  this.Address3 = options.Address3;
  this.dob = options.dob;
  this.gender = options.gender;
  this.consultant = options.consultant;
  this.illness = options.illness;
  this.clinicalInfo = options.clinicalInfo;
  this.certified = options.certified;
  this.tpBilling = options.tpBilling;
  this.encounterId = options.encounterId;  
};
